/**
 * Created by fujisho on 15/02/23.
 * 
 * UserInfo has user's name (screen name), homeroom,   
 * 
 * 
 * 
 * 
 * 
 */

var placeArray = new Array("帰宅", "学外", "学内", "新谷研究室",  "教官室（新谷）", "教官室（大囿）", "教官室（白松）", "ゼミ室", "秘書室", "院部屋", "学部部屋", "図書館", "講義", "食事");

/**
 * 
 * @param board_name
 * @param grade
 * @param place 現在地
 * @constructor
 */
var UserInfo = function(board_name, grade, place_number) {
  this.slack_name = board_name;
  this.board_name = board_name;
  this.place = placeArray[place_number];
  
  switch (grade){
    case 0:
      switch (board_name) {
        case 'toramatsu3':
          this.slack_name = 'tora';
          this.home_room = '教官室（新谷）';
          break;
        case 'ozn202a':
          this.slack_name = 'ozono';
          this.home_room = '教官室（大囿）';
          break;
        case 'siramatu':
          this.home_room = '教官室（白松）';
          break;
        default :
          break;
      }
      break;
    case 1:
    case 2:
    case 3:
      this.home_room = '院部屋';
      break;
    case 4:
      this.home_room = '学部部屋';
      break;
    default :
      break;
  }
  
  
  
};

UserInfo.prototype.setPlaceWithNumber = function (place_number) {
	this.place = placeArray[place_number];
};

UserInfo.prototype.setPlaceWithString = function (string) {
  this.place = string;
  
};

module.exports = UserInfo;
