// This is a simple example of how to use the slack-client module. It creates a
// bot that responds to all messages in all channels it is in with a reversed
// string of the text received.
//
// To run, copy your token below, then:
//	npm install
// 	cd examples
// 	node simple.js

// Note: in a normal implementation with the package loaded from NPM, you won't have to do this
require('coffee-script/register');

var Slack = require('..');

var io = require('socket.io');
var request = require('request');
var http = require('http');
var execsyncs = require('execsyncs');
var UserInfo = require('./userinfo');

var token = 'xoxb-3806602846-2a7msk9AoUv3FN55bDkx4fDL', // Add a bot at https://my.slack.com/services/new/bot and copy the token here.
	autoReconnect = true,
	autoMark = true;

var slack = new Slack(token, autoReconnect, autoMark);

var hoge = require('./server');


var boardData = "" + execsyncs('curl -s http://winter.ics.nitech.ac.jp:8080');
boardData = JSON.parse(boardData);
var userInfoArray = initUserInfo(boardData);




slack.on('open', function () {

	var channels = [],
		groups = [],
		unreads = slack.getUnreadCount(),
		key;

	for (key in slack.channels) {
		if (slack.channels[key].is_member) {
			channels.push('#' + slack.channels[key].name);
		}
	}

	for (key in slack.groups) {
		if (slack.groups[key].is_open && !slack.groups[key].is_archived) {
			groups.push(slack.groups[key].name);
		}
	}

  for (key in slack.users) {
    if (slack.users[key].presence === 'active') {
      for (var i = 0; i < userInfoArray.length; i++) {
        var user = userInfoArray[i];
        if (slack.users[key].name == user.slack_name && user.place == '帰宅') {
          moveToralaBoard(user.board_name, user.home_room);
          break;
        }
      }
    }
  }
  
	console.log('Welcome to Slack. You are @%s of %s', slack.self.name, slack.team.name);
	console.log('You are in: %s', channels.join(', '));
	console.log('As well as: %s', groups.join(', '));
	console.log('You have %s unread ' + (unreads === 1 ? 'message' : 'messages'), unreads);


});

slack.on('message', function (message) {

	//var type = message.type,
	//	channel = slack.getChannelGroupOrDMByID(message.channel),
	//	user = slack.getUserByID(message.user),
	//	time = message.ts,
	//	text = message.text,
	//	response = '';
  //
	//console.log('Received: %s %s @%s %s "%s"', type, (channel.is_channel ? '#' : '') + channel.name, user.name, time, text);
  //
	//// Respond to messages with the reverse of the text received.
  //
	//if (type === 'message') {
  //
	//	response = text.split('').reverse().join('');
	//	channel.send(response);
	//	console.log('@%s responded with "%s"', slack.self.name, response);
	//}
});


/**
 * なぜかdata.presenceの値(active, away)が逆になっている
 */
slack.on('presenceChange', function (data) {
	var slack_name = data.name,
		presence = data.presence,
		is_bot = data.is_bot;
  

  console.log(slack_name + ' is not ' + presence);
  
  
  
  //if (!is_bot) {
  //  if (presence === 'away') { //オンライン
  //    for (var i = 0; i < userInfoArray.length; i++) {
  //      var user = userInfoArray[i];
  //      if (user.slack_name == slack_name) {
  //        moveToralaBoard(user.board_name, user.home_room);
  //        break;
  //      }
  //    }
  
  //  } else if (presence === 'active') {
  //    var h = new Date().getHours();
  //    if (h > 15) {
  //      for (var i = 0; i < userInfoArray.length; i++) {
  //        var user = userInfoArray[i];2
  //        if (user.slack_name == slack_name) {
  //          moveToralaBoard(user.board_name, '帰宅');
  //          break;
  //        }
  //      }
  //    }
  //  }
  //}
  
});


slack.on('error', function (error) {
	console.error('Error: %s', error);
});

slack.login();


/**
 * * 
 * @param data
 * @returns {Array}
 */
function initUserInfo(data) {
  var userInfoArray = new Array();
  for (var i = 0; i < data.length; i++) {
    var user = data[i];
    var userInfo = new UserInfo(user.id, user.colorindex, user.whereaboutindex);
    userInfoArray.push(userInfo);
  }
  return userInfoArray;
}


function moveToralaBoard(user, place) {
  var url = 'http://winter.ics.nitech.ac.jp:3001/?mode=simple&update=' + user + '&status=available&message=' + encodeURIComponent(place);
  http.get(url, function (res) {
    console.log(user + ' moved to ' + place);
  }).on('error', function (err) {
    console.log('err : ' + err.message);
  });
  
}


//function loadData(callback) {
//  var json;
//	var options = {
//		url: 'http://winter.ics.nitech.ac.jp:8080',
//		json: true
//	};
//	request.get(options, json = function (error, response, body) {
//		if (!error) {
//			//console.log(body);
//			callback(body);
//		} else {
//			console.log('error: ' + error);
//		}
//	});
//
//}

